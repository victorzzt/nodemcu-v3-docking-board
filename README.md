# NodeMCU V3 Docking Board
* Download to fabricate: [release](./release/ESP8266DockingCAM.zip?at=master)

* Update 20191124
    * Added a rendered image and top down mechanical dimension for people who need to do mount or casing
* Update 20190903
    * Minor bug fix, power pin for SPI obstructing Nut of the top left mounting hole
    * Logo switched to MakeBig Initiative (www.makerfairehongkong.com)
* Update 20190814
    * Major bug fix, for ESP12E module with the CP210x dev-kit, the 2 reserved wire shouldn't be connected. The one below A0 is the test point for the actually analog pin (1.0v) testpoint for the voltage division resistor network.
    * Minor updates 
        * Added version number to the back of the board
        * Added visual silk screen for prototype area
        * Added pin labels for prototyping area
        * Moved custom 6pin with custom connection further away from 5v tap points 25mils to be able to use JST XH-2.54 connector(foot print same with 2.54 header but the plastic casing bigger) without obstruction
        * Moved 1812 PTC away 25mil from 2.54mm pin headers
        * Added Slits for securing Power Switch wire
    * Additional remark
        * As I found out that different modules have different orientation for SPI/I2C pin order, I see no point adjusting it for a specific order, use with caution by twisting 6pin/4pin cable wire connection to not blow anything up.
        * However, most I2C modules seems to put SDA closer to power ,so switched to that order, however the power pin for 4pin I2C isn't really standardized. (Now is VCC-GND-SDA-SCL)
        * Use the NodemcuDockingV1,0.cam to output including the drilling layer to profile
* Update 20190729
    * As I noticed that fritzing have some serious bug regarding pcb view import (Has a ghost connection pad in the average center of the pads in the same net)
        * AI has been updated so the export doesn't directly work
        * Inkscape has the same problem
    * Migrating the whole project into Eagle (which is still free for hobbiests)
        * However it will mean the software will be harder to operate by designer
    * Eagle project not presented, just brd and sch
        * Used library is in the (Fritzing Library)[https://bitbucket.org/victorzzt/fritzing-parts-bins/src/master/] Might need to change the name


#### This is a reinvent of a NodeMCU V3 board. Board like [this one](https://www.aliexpress.com/i/32670763311.html)
*Why I'm doing this:*

* There's lack of consistency of many vendor of this board, which many even had the L293DD's en pin pulled low
    * Many cases a half-bridge isn't really needed.
    * It's better to use extension board as this prototyping tool is not intended for vehicles, but for installation.
    * So form factor isn't the main concern, easy connection is.
* The pin head of SPI and power isn't really optimal (my personal opinion)
* The LDO won't be able to deliver for more than 2 standard servos ( experience )
    * phasing out 12V connector+LDO
    * Add a type-c and screw terminal to be used with external PSU 1 / Powerbank
* Possible converter block for external i2c/spi hardware with screw terminal/JST connectors
    * For better connectors, please also suggest, can make variations.
* Made in non-professional EDA - Fritzing
    * As this is mostly for designer to prototype and not intended for industrial solution, it's better to pick software with lower learning curve
    * Future can consider transplanting to Eagle/Design Spark
    
#### May need fix
* SPI / I2C pin order
    * Wanted to follow ICSP standard, however RST/CS inconsistency might as well use a custom layout
    * To be optimized (for Ver 1.0 will use self assigned layout)

#### BOM (Just reference)
* PTC 
    * [1812 version](https://item.taobao.com/item.htm?spm=a230r.1.14.11.5a76500dNTodsU&id=596914181058&ns=1&abbucket=7#detail), use larger amp if you have more than 1 servo (700~800mA per standard sized RC servo, peak)
* DIP SW 2 ( for I2c pullup )
* Schottkey Diode ( either or )
    * DIP
    * Larger SMD